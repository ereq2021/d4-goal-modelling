{
  "actors": [
    {
      "id": "1e573b25-35ce-4571-8293-03b0e6f59212",
      "text": "Empresa associada",
      "type": "istar.Actor",
      "x": 320,
      "y": 53,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "a149e058-f191-430c-a532-738c94cbf456",
          "text": "Pagar Quotas",
          "type": "istar.Task",
          "x": 796,
          "y": 261,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "38eade73-dde4-40e3-b83c-e1a49008e18c",
          "text": "Certificado alvará obtido",
          "type": "istar.Goal",
          "x": 869,
          "y": 63,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "2be61bf2-7f81-47f9-b2df-b9bb93be53bf",
          "text": "Certificado AVAC obtido",
          "type": "istar.Goal",
          "x": 994,
          "y": 63,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "87561a6a-cef9-4bec-90a6-a54c02353e39",
          "text": "Selo APIRAC - Ar Saudável obtido",
          "type": "istar.Goal",
          "x": 1116,
          "y": 59,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "4f55bee0-d3df-48d2-8101-2e57f7fde690",
          "text": "Criar canal de comunicação",
          "type": "istar.Task",
          "x": 500,
          "y": 388,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "25804597-0266-4182-99e1-0d91d1ec354d",
          "text": "Requisitos para obtenção do certificado satisfeitos",
          "type": "istar.Goal",
          "x": 1043,
          "y": 378,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "f039d737-2802-492d-a497-f68dc9d18522",
          "text": "Duvidas de caráter jurídico esclarecidas",
          "type": "istar.Goal",
          "x": 587,
          "y": 62,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "0d73b00d-30b2-4999-99c5-60e7f46cfc4e",
          "text": "Duvidas de caráter técnico esclarecidas",
          "type": "istar.Goal",
          "x": 395,
          "y": 53,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "953d9eef-beae-4d0e-a52c-5632232c661c",
          "text": "Pedir aconselhamento técnico",
          "type": "istar.Task",
          "x": 399,
          "y": 254,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "d05fe910-5a86-4f27-a751-28f586d5318a",
          "text": "pedir aconselhamento jurídico",
          "type": "istar.Task",
          "x": 597,
          "y": 254,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "83cb291e-8f1d-4b7a-9896-63ad5e08970a",
          "text": "Enviar as duvidas de carácter jurídico",
          "type": "istar.Task",
          "x": 694,
          "y": 367,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "0501a0f0-499c-46e6-84a1-25fdb5e9b539",
          "text": "Enviar as duvidas de carácter técnico",
          "type": "istar.Task",
          "x": 320,
          "y": 372,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "6c1664b4-cb4f-4ddb-a6c6-faf98e19118b",
          "text": "Pedir certificação",
          "type": "istar.Task",
          "x": 875,
          "y": 396,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "6ef9758b-b2c8-490d-999a-71be7d9651d5",
          "text": "Publicitação obtida",
          "type": "istar.Goal",
          "x": 1293,
          "y": 58,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "ae4ef88f-b997-41dc-be94-110633102ca8",
          "text": "Escolher tipo de publicitação",
          "type": "istar.Task",
          "x": 1280,
          "y": 238,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "0be1724f-8b01-4909-9c67-5b9736227c97",
          "text": "Solicitar Publicitação",
          "type": "istar.Task",
          "x": 1170,
          "y": 239,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "40248a4a-cefc-4512-8a85-7311a1202442",
          "text": "Enviar dados para publicitação",
          "type": "istar.Task",
          "x": 1411,
          "y": 235,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "487fafb9-30dd-4156-a60d-d414adc05ae2",
      "text": "APIRAC",
      "type": "istar.Agent",
      "x": 2351,
      "y": 292,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "380a022d-b664-4b27-97d5-142550db6f62",
      "text": "APIEF",
      "type": "istar.Agent",
      "x": 2388,
      "y": 131,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "1db70df2-7415-4f67-b50e-091fd4a968e0",
      "text": "Dep. Jurídico",
      "type": "istar.Agent",
      "x": 2043,
      "y": 29,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "17430d59-9e39-41bc-bb59-d15890306f58",
      "text": "Dep. Técnico",
      "type": "istar.Agent",
      "x": 1670,
      "y": 185,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "4bd5b67d-589c-4d03-9285-7b751c4d5761",
      "text": "Dep. Administrativo e Financeiro",
      "type": "istar.Agent",
      "x": 1911,
      "y": 1045,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "8c07f104-ee94-46d8-8b31-f587ba3f84ef",
          "text": "processar pagamento",
          "type": "istar.Task",
          "x": 2067,
          "y": 1122,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "04e197df-5ebf-44d6-820f-9bad5290b416",
          "text": "Emitir fatura",
          "type": "istar.Task",
          "x": 2073,
          "y": 1261,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "bec5a869-7c90-44fe-8b54-0be1a47d0929",
          "text": "Dados de pagamento validados",
          "type": "istar.Goal",
          "x": 1918,
          "y": 1250,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "6cdaeba4-1989-44f7-bcbc-03212f158731",
          "text": "Notificar empresa associada",
          "type": "istar.Task",
          "x": 2202,
          "y": 1250,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "6fd1b666-effb-4256-8ea2-11c7c55ca8fd",
      "text": "Dep. de Comunicação e Imagem",
      "type": "istar.Agent",
      "x": 1611,
      "y": 407,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "ab678a6f-f18d-4de9-9b78-7998dd063930",
          "text": "Newsletter atualizado",
          "type": "istar.Goal",
          "x": 1860,
          "y": 410,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "33a75d90-8206-4663-80c7-933df5396054",
          "text": "Submeter novas noticias",
          "type": "istar.Task",
          "x": 1859,
          "y": 617,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "7c7329fe-4662-409e-9c1a-ad0a10677532",
          "text": "Responder ao pedido",
          "type": "istar.Task",
          "x": 1611,
          "y": 545,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "76a410d1-8f47-4bef-bf96-aa48d186d0f6",
          "text": "publicitação realizada",
          "type": "istar.Goal",
          "x": 1710,
          "y": 409,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "a62b633a-fbcb-46d5-ab7a-189944d22377",
      "text": "Dep. de Formação",
      "type": "istar.Agent",
      "x": 1828,
      "y": 60,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "09f2b96e-529a-466a-8edc-f39c7f7a37d0",
      "text": "Website da APIRAC",
      "type": "istar.Agent",
      "x": 1180,
      "y": 778,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "3dc0e810-dbf0-4219-baea-e7522b7b0a00",
          "text": "Canal estabelecido",
          "type": "istar.Goal",
          "x": 1477,
          "y": 845,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "01ec5fe8-81f8-4dd4-bec1-97089a7a5bd1",
          "text": "Mensagens entreges",
          "type": "istar.Goal",
          "x": 1820,
          "y": 791,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "3f8ce995-73d0-4688-b8bb-33dcbd71ffc3",
          "text": "Novas mensagens notificadas",
          "type": "istar.Goal",
          "x": 1615,
          "y": 778,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "c0e0c920-1624-4a26-ab92-ee193bb60118",
          "text": "Processar formulário",
          "type": "istar.Task",
          "x": 1257,
          "y": 872,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "a0c7e01a-cae0-4351-ac4e-a6a479d1f290",
          "text": "Dados validados",
          "type": "istar.Goal",
          "x": 1190,
          "y": 968,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "64af61e1-a3dc-4f47-b285-6a6abf7f53e1",
          "text": "enviar notificação ao departamento financeiro",
          "type": "istar.Task",
          "x": 1337,
          "y": 953,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "6b141c49-1b19-40bd-9a05-b86b3ba544b8",
      "text": "Empresa não associada",
      "type": "istar.Actor",
      "x": 336,
      "y": 554,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "6081471e-6a84-49c7-bf7a-6d1cd88be774",
          "text": "Preencher formulário em papel",
          "type": "istar.Task",
          "x": 372,
          "y": 792,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "2029f85e-d744-489a-8851-a206be3cc78c",
          "text": "Sem erros",
          "type": "istar.Quality",
          "x": 460,
          "y": 914,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "f209d583-bed4-4df7-8ecb-8da295044b16",
          "text": "Formulário de adesão preenchido",
          "type": "istar.Goal",
          "x": 448,
          "y": 700,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "bf4a51d9-2d40-41b5-bc0b-8e7d885eb4d2",
          "text": "Preencher formulário online",
          "type": "istar.Task",
          "x": 526,
          "y": 796,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "bc7776d5-e641-4ef7-8262-f3425b6dce21",
          "text": "Submeter formulário",
          "type": "istar.Task",
          "x": 616,
          "y": 712,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "27897595-6b6f-41f5-b653-077d7a0771a1",
          "text": "Associação estabelecida",
          "type": "istar.Goal",
          "x": 621,
          "y": 588,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    }
  ],
  "orphans": [],
  "dependencies": [
    {
      "id": "da39ea5a-b1a8-485a-baf4-0a4573bbb2b6",
      "text": "Pagamento processado",
      "type": "istar.Goal",
      "x": 799,
      "y": 1104,
      "customProperties": {
        "Description": ""
      },
      "source": "a149e058-f191-430c-a532-738c94cbf456",
      "target": "8c07f104-ee94-46d8-8b31-f587ba3f84ef"
    },
    {
      "id": "7562b453-d49d-4af7-934a-3d56bce1f946",
      "text": "Pedido processado",
      "type": "istar.Goal",
      "x": 1172,
      "y": 544,
      "customProperties": {
        "Description": ""
      },
      "source": "0be1724f-8b01-4909-9c67-5b9736227c97",
      "target": "7c7329fe-4662-409e-9c1a-ad0a10677532"
    },
    {
      "id": "adbdd0f9-a7af-4fcf-b1ca-84cb6c9a717b",
      "text": "Formulário processado",
      "type": "istar.Goal",
      "x": 947,
      "y": 800,
      "customProperties": {
        "Description": ""
      },
      "source": "bc7776d5-e641-4ef7-8262-f3425b6dce21",
      "target": "c0e0c920-1624-4a26-ab92-ee193bb60118"
    }
  ],
  "links": [
    {
      "id": "5e319c82-de60-433f-af2f-aee330ae5629",
      "type": "istar.ParticipatesInLink",
      "source": "1db70df2-7415-4f67-b50e-091fd4a968e0",
      "target": "487fafb9-30dd-4156-a60d-d414adc05ae2"
    },
    {
      "id": "97b24db3-1cad-4dd8-b59d-a8e37794312d",
      "type": "istar.ParticipatesInLink",
      "source": "a62b633a-fbcb-46d5-ab7a-189944d22377",
      "target": "487fafb9-30dd-4156-a60d-d414adc05ae2"
    },
    {
      "id": "f8847ec8-3a43-489a-aca7-54d152eca98f",
      "type": "istar.ParticipatesInLink",
      "source": "6fd1b666-effb-4256-8ea2-11c7c55ca8fd",
      "target": "487fafb9-30dd-4156-a60d-d414adc05ae2"
    },
    {
      "id": "132d2426-5aa7-4399-ae2d-113e8216ffda",
      "type": "istar.ParticipatesInLink",
      "source": "17430d59-9e39-41bc-bb59-d15890306f58",
      "target": "487fafb9-30dd-4156-a60d-d414adc05ae2"
    },
    {
      "id": "caca4b4c-6732-4e06-9f34-e773eeb92db8",
      "type": "istar.ParticipatesInLink",
      "source": "4bd5b67d-589c-4d03-9285-7b751c4d5761",
      "target": "487fafb9-30dd-4156-a60d-d414adc05ae2"
    },
    {
      "id": "517be5c6-1235-4f9b-9cd7-5c20cdd0c9c5",
      "type": "istar.AndRefinementLink",
      "source": "a149e058-f191-430c-a532-738c94cbf456",
      "target": "87561a6a-cef9-4bec-90a6-a54c02353e39"
    },
    {
      "id": "9d86bc86-e764-4d81-a7db-3610e06ecb33",
      "type": "istar.AndRefinementLink",
      "source": "a149e058-f191-430c-a532-738c94cbf456",
      "target": "2be61bf2-7f81-47f9-b2df-b9bb93be53bf"
    },
    {
      "id": "0edaca1c-89e1-4fb8-a409-166abf591d42",
      "type": "istar.AndRefinementLink",
      "source": "a149e058-f191-430c-a532-738c94cbf456",
      "target": "38eade73-dde4-40e3-b83c-e1a49008e18c"
    },
    {
      "id": "26418db7-f6ec-4aea-8b9a-06052a6e7151",
      "type": "istar.ContributionLink",
      "source": "6081471e-6a84-49c7-bf7a-6d1cd88be774",
      "target": "2029f85e-d744-489a-8851-a206be3cc78c",
      "label": "hurt"
    },
    {
      "id": "3ae76d17-6245-48f6-9159-65aae409e7e2",
      "type": "istar.AndRefinementLink",
      "source": "25804597-0266-4182-99e1-0d91d1ec354d",
      "target": "38eade73-dde4-40e3-b83c-e1a49008e18c"
    },
    {
      "id": "12d0e96c-4b10-4f7c-8cbf-0cb970a1ad73",
      "type": "istar.AndRefinementLink",
      "source": "25804597-0266-4182-99e1-0d91d1ec354d",
      "target": "2be61bf2-7f81-47f9-b2df-b9bb93be53bf"
    },
    {
      "id": "14e81e25-5214-4509-bc57-94f90d772f4d",
      "type": "istar.AndRefinementLink",
      "source": "25804597-0266-4182-99e1-0d91d1ec354d",
      "target": "87561a6a-cef9-4bec-90a6-a54c02353e39"
    },
    {
      "id": "5eac5cad-0a3e-4a1f-a55f-b770d4eade4d",
      "type": "istar.AndRefinementLink",
      "source": "953d9eef-beae-4d0e-a52c-5632232c661c",
      "target": "0d73b00d-30b2-4999-99c5-60e7f46cfc4e"
    },
    {
      "id": "681aef52-bbc0-40f5-890b-613d31f6ef9e",
      "type": "istar.AndRefinementLink",
      "source": "d05fe910-5a86-4f27-a751-28f586d5318a",
      "target": "f039d737-2802-492d-a497-f68dc9d18522"
    },
    {
      "id": "d2021d74-5dcf-4df6-9081-265dc5d65500",
      "type": "istar.AndRefinementLink",
      "source": "4f55bee0-d3df-48d2-8101-2e57f7fde690",
      "target": "953d9eef-beae-4d0e-a52c-5632232c661c"
    },
    {
      "id": "846e34fa-4f29-45a5-b9d6-70684f434e60",
      "type": "istar.AndRefinementLink",
      "source": "4f55bee0-d3df-48d2-8101-2e57f7fde690",
      "target": "d05fe910-5a86-4f27-a751-28f586d5318a"
    },
    {
      "id": "3bebfd54-e977-4429-9266-fff41afa505c",
      "type": "istar.AndRefinementLink",
      "source": "a149e058-f191-430c-a532-738c94cbf456",
      "target": "0d73b00d-30b2-4999-99c5-60e7f46cfc4e"
    },
    {
      "id": "e9db800d-527f-46a6-83dc-3e63ff704890",
      "type": "istar.AndRefinementLink",
      "source": "a149e058-f191-430c-a532-738c94cbf456",
      "target": "f039d737-2802-492d-a497-f68dc9d18522"
    },
    {
      "id": "90be6071-dd65-450a-9531-2a8ed94342e5",
      "type": "istar.AndRefinementLink",
      "source": "83cb291e-8f1d-4b7a-9896-63ad5e08970a",
      "target": "d05fe910-5a86-4f27-a751-28f586d5318a"
    },
    {
      "id": "6a800e13-e0a6-462a-a906-c0af3fc36fad",
      "type": "istar.AndRefinementLink",
      "source": "0501a0f0-499c-46e6-84a1-25fdb5e9b539",
      "target": "953d9eef-beae-4d0e-a52c-5632232c661c"
    },
    {
      "id": "6af9523b-7e30-48de-889b-68075a4d760f",
      "type": "istar.AndRefinementLink",
      "source": "33a75d90-8206-4663-80c7-933df5396054",
      "target": "ab678a6f-f18d-4de9-9b78-7998dd063930"
    },
    {
      "id": "436acd63-d982-4eca-b425-fec5560947a0",
      "type": "istar.ContributionLink",
      "source": "bf4a51d9-2d40-41b5-bc0b-8e7d885eb4d2",
      "target": "2029f85e-d744-489a-8851-a206be3cc78c",
      "label": "help"
    },
    {
      "id": "d79550aa-7c84-4b78-bb7d-12ad7c01c934",
      "type": "istar.OrRefinementLink",
      "source": "6081471e-6a84-49c7-bf7a-6d1cd88be774",
      "target": "f209d583-bed4-4df7-8ecb-8da295044b16"
    },
    {
      "id": "998593f8-284b-43d1-a9e6-e2bf6b7475ee",
      "type": "istar.OrRefinementLink",
      "source": "bf4a51d9-2d40-41b5-bc0b-8e7d885eb4d2",
      "target": "f209d583-bed4-4df7-8ecb-8da295044b16"
    },
    {
      "id": "3c909446-8434-43be-8cfb-bef4d99053a8",
      "type": "istar.AndRefinementLink",
      "source": "f209d583-bed4-4df7-8ecb-8da295044b16",
      "target": "27897595-6b6f-41f5-b653-077d7a0771a1"
    },
    {
      "id": "574ce548-eece-4a3e-a089-5dd17e8480e4",
      "type": "istar.AndRefinementLink",
      "source": "bc7776d5-e641-4ef7-8262-f3425b6dce21",
      "target": "27897595-6b6f-41f5-b653-077d7a0771a1"
    },
    {
      "id": "fee61044-dd0b-45c8-9908-a766263062b1",
      "type": "istar.AndRefinementLink",
      "source": "6c1664b4-cb4f-4ddb-a6c6-faf98e19118b",
      "target": "38eade73-dde4-40e3-b83c-e1a49008e18c"
    },
    {
      "id": "e75d46d6-7a62-4103-b30a-5d309982d492",
      "type": "istar.AndRefinementLink",
      "source": "6c1664b4-cb4f-4ddb-a6c6-faf98e19118b",
      "target": "2be61bf2-7f81-47f9-b2df-b9bb93be53bf"
    },
    {
      "id": "47a18d1b-959d-4e22-805b-feb82fa14879",
      "type": "istar.AndRefinementLink",
      "source": "6c1664b4-cb4f-4ddb-a6c6-faf98e19118b",
      "target": "87561a6a-cef9-4bec-90a6-a54c02353e39"
    },
    {
      "id": "30df15a0-2e34-4281-be70-23bf1097c243",
      "type": "istar.DependencyLink",
      "source": "a149e058-f191-430c-a532-738c94cbf456",
      "target": "da39ea5a-b1a8-485a-baf4-0a4573bbb2b6"
    },
    {
      "id": "75abd554-d120-404e-8bbe-4ab0ffce2a5f",
      "type": "istar.DependencyLink",
      "source": "da39ea5a-b1a8-485a-baf4-0a4573bbb2b6",
      "target": "8c07f104-ee94-46d8-8b31-f587ba3f84ef"
    },
    {
      "id": "b9b0f409-acf6-40d0-aed2-f69e0df0bc17",
      "type": "istar.AndRefinementLink",
      "source": "0be1724f-8b01-4909-9c67-5b9736227c97",
      "target": "6ef9758b-b2c8-490d-999a-71be7d9651d5"
    },
    {
      "id": "b9bed471-9999-4c71-ac49-e2a239f04c2e",
      "type": "istar.AndRefinementLink",
      "source": "ae4ef88f-b997-41dc-be94-110633102ca8",
      "target": "6ef9758b-b2c8-490d-999a-71be7d9651d5"
    },
    {
      "id": "6dd92a7d-f0e9-4823-b2fa-49ecc411933d",
      "type": "istar.DependencyLink",
      "source": "0be1724f-8b01-4909-9c67-5b9736227c97",
      "target": "7562b453-d49d-4af7-934a-3d56bce1f946"
    },
    {
      "id": "6997cd5b-a6e3-451b-b0e1-4affd30c88fa",
      "type": "istar.DependencyLink",
      "source": "7562b453-d49d-4af7-934a-3d56bce1f946",
      "target": "7c7329fe-4662-409e-9c1a-ad0a10677532"
    },
    {
      "id": "6ec056d1-2adb-4f35-b9e6-a25fcfc2d4bf",
      "type": "istar.ParticipatesInLink",
      "source": "09f2b96e-529a-466a-8edc-f39c7f7a37d0",
      "target": "487fafb9-30dd-4156-a60d-d414adc05ae2"
    },
    {
      "id": "f945038b-c89b-4181-83be-81565f88bc87",
      "type": "istar.DependencyLink",
      "source": "bc7776d5-e641-4ef7-8262-f3425b6dce21",
      "target": "adbdd0f9-a7af-4fcf-b1ca-84cb6c9a717b"
    },
    {
      "id": "d8f2a74b-c9a6-4678-a511-af92722cec45",
      "type": "istar.DependencyLink",
      "source": "adbdd0f9-a7af-4fcf-b1ca-84cb6c9a717b",
      "target": "c0e0c920-1624-4a26-ab92-ee193bb60118"
    },
    {
      "id": "ed6277da-0a20-4f4a-bf4a-e52d78c71d1c",
      "type": "istar.AndRefinementLink",
      "source": "a0c7e01a-cae0-4351-ac4e-a6a479d1f290",
      "target": "c0e0c920-1624-4a26-ab92-ee193bb60118"
    },
    {
      "id": "14029691-fe53-4ab1-808b-3e7484da27b2",
      "type": "istar.AndRefinementLink",
      "source": "64af61e1-a3dc-4f47-b285-6a6abf7f53e1",
      "target": "c0e0c920-1624-4a26-ab92-ee193bb60118"
    },
    {
      "id": "7341c7ec-d3e7-4e92-b7c6-399c29b3a69b",
      "type": "istar.AndRefinementLink",
      "source": "04e197df-5ebf-44d6-820f-9bad5290b416",
      "target": "8c07f104-ee94-46d8-8b31-f587ba3f84ef"
    },
    {
      "id": "99810f58-d37d-4cbe-bd33-cdab02d76241",
      "type": "istar.AndRefinementLink",
      "source": "bec5a869-7c90-44fe-8b54-0be1a47d0929",
      "target": "8c07f104-ee94-46d8-8b31-f587ba3f84ef"
    },
    {
      "id": "e3b900c9-871a-450b-a155-7d47451ae89f",
      "type": "istar.AndRefinementLink",
      "source": "6cdaeba4-1989-44f7-bcbc-03212f158731",
      "target": "8c07f104-ee94-46d8-8b31-f587ba3f84ef"
    },
    {
      "id": "21e4a668-6e5d-430d-a28f-d1bc77f747e2",
      "type": "istar.AndRefinementLink",
      "source": "7c7329fe-4662-409e-9c1a-ad0a10677532",
      "target": "76a410d1-8f47-4bef-bf96-aa48d186d0f6"
    },
    {
      "id": "9ab3af66-0806-4192-ab8c-bfa9a3e86515",
      "type": "istar.AndRefinementLink",
      "source": "40248a4a-cefc-4512-8a85-7311a1202442",
      "target": "6ef9758b-b2c8-490d-999a-71be7d9651d5"
    }
  ],
  "display": {
    "87561a6a-cef9-4bec-90a6-a54c02353e39": {
      "width": 103.88748168945312,
      "height": 51.79999542236328
    },
    "25804597-0266-4182-99e1-0d91d1ec354d": {
      "width": 136.88748168945312,
      "height": 66.19999504089355
    },
    "f039d737-2802-492d-a497-f68dc9d18522": {
      "width": 119.88748168945312,
      "height": 59.199995040893555
    },
    "0d73b00d-30b2-4999-99c5-60e7f46cfc4e": {
      "width": 127.88748168945312,
      "height": 69.19999504089355
    },
    "953d9eef-beae-4d0e-a52c-5632232c661c": {
      "width": 111.38748168945312,
      "height": 43.199995040893555
    },
    "d05fe910-5a86-4f27-a751-28f586d5318a": {
      "width": 109.38748168945312,
      "height": 54.199995040893555
    },
    "83cb291e-8f1d-4b7a-9896-63ad5e08970a": {
      "width": 122.88748168945312,
      "height": 56.199995040893555
    },
    "0501a0f0-499c-46e6-84a1-25fdb5e9b539": {
      "width": 121.88748168945312,
      "height": 60.199995040893555
    },
    "ae4ef88f-b997-41dc-be94-110633102ca8": {
      "width": 109.28750610351562,
      "height": 46.199995040893555
    },
    "40248a4a-cefc-4512-8a85-7311a1202442": {
      "width": 119.609375,
      "height": 50
    },
    "bec5a869-7c90-44fe-8b54-0be1a47d0929": {
      "width": 98.609375,
      "height": 46
    },
    "6cdaeba4-1989-44f7-bcbc-03212f158731": {
      "width": 108.609375,
      "height": 48
    },
    "ab678a6f-f18d-4de9-9b78-7998dd063930": {
      "width": 102.68746948242188,
      "height": 44.199989318847656
    },
    "33a75d90-8206-4663-80c7-933df5396054": {
      "width": 110.68746948242188,
      "height": 48.199989318847656
    },
    "76a410d1-8f47-4bef-bf96-aa48d186d0f6": {
      "width": 92.609375,
      "height": 42
    },
    "01ec5fe8-81f8-4dd4-bec1-97089a7a5bd1": {
      "width": 96.48748779296875,
      "height": 46.00000762939453
    },
    "3f8ce995-73d0-4688-b8bb-33dcbd71ffc3": {
      "width": 127.88748168945312,
      "height": 58.99999237060547
    },
    "64af61e1-a3dc-4f47-b285-6a6abf7f53e1": {
      "width": 120.609375,
      "height": 52
    },
    "6081471e-6a84-49c7-bf7a-6d1cd88be774": {
      "width": 112.68748474121094,
      "height": 56.99999237060547
    },
    "f209d583-bed4-4df7-8ecb-8da295044b16": {
      "width": 112.68748474121094,
      "height": 51.49999237060547
    },
    "bf4a51d9-2d40-41b5-bc0b-8e7d885eb4d2": {
      "width": 99.68748474121094,
      "height": 47.59998321533203
    },
    "487fafb9-30dd-4156-a60d-d414adc05ae2": {
      "collapsed": true
    }
  },
  "tool": "pistar.2.0.0",
  "istar": "2.0",
  "saveDate": "Thu, 18 Nov 2021 23:01:04 GMT",
  "diagram": {
    "width": 3412,
    "height": 1350,
    "name": "Welcome Model",
    "customProperties": {
      "Description": "Welcome to the piStar tool! This model describe some of the recent improvements in the tool.\n\nFor help using this tool, please check the Help menu above"
    }
  }
}